package com.ecommerce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
//@RefreshScope
@EnableEurekaClient
public class EcommerceUserinfoServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcommerceUserinfoServiceApplication.class, args);
		System.out.println("user-servie is running");
	}
	
	@Bean
	public RestTemplate getRestTemplate() {
	   return new RestTemplate();
	}

}
