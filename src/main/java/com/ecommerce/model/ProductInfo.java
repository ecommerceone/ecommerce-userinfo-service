package com.ecommerce.model;

import lombok.Data;

@Data
public class ProductInfo {
	
	private Long productId;

	private String productName;
	
	private String productDescription;
	
	private String productType;
	
	private String color;
	
	private String sellerId;
	
	private String brand;
	
	private Integer replacementPolicy;
	
	private Double originalPrice;

	private Double discount;
	
	private Double sellingPrice;
	
	private String photoPath;
	
	
}
