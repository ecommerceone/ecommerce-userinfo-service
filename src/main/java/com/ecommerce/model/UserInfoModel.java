package com.ecommerce.model;

import lombok.Data;

@Data
public class UserInfoModel {
	
	private String firstName;
	
	private String lastName;

	private String gender;
	
	private String emailId;
	
	private String mobile;

}
