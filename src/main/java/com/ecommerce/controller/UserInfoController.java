package com.ecommerce.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ecommerce.entity.UserInfo;
import com.ecommerce.model.ProductInfo;
import com.ecommerce.model.UserInfoModel;
import com.ecommerce.service.UserInfoService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("userInfo")
@Slf4j
public class UserInfoController {

	@Autowired
	UserInfoService userInfoService;
	
	@PostMapping("user")
	public ResponseEntity<UserInfo> addUser(@RequestBody UserInfoModel userInfoModel) {
		log.info("Inside addUser Controller");
		UserInfo userInfo = userInfoService.addUser(userInfoModel);
		return new ResponseEntity<>(userInfo, HttpStatus.OK);

	}
	
	@GetMapping("users")
	public ResponseEntity<List<UserInfo>> getAllUsers(){
		log.info("Inside getAllUsers Controller");
		List<UserInfo> userInfoLst = userInfoService.getAllUsers();
		return new ResponseEntity<>(userInfoLst, HttpStatus.OK);
	}

	@GetMapping("user/{userId}")
	public ResponseEntity<UserInfo> getUserById(@PathVariable Long userId) {
		log.info("Inside getUserById Controller");
		UserInfo userInfo = userInfoService.getUserById(userId);
		return new ResponseEntity<>(userInfo, HttpStatus.OK);
	}
	
	@GetMapping("productViaUser")
	public ResponseEntity<List<ProductInfo>> getAllProduct(){
		log.info("Inside getAllUsers Controller");
		List<ProductInfo> productLst = userInfoService.getProducts();
		return new ResponseEntity<>(productLst, HttpStatus.OK);
	}
}
