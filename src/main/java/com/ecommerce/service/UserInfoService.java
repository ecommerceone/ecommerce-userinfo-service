package com.ecommerce.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.ecommerce.entity.UserInfo;
import com.ecommerce.model.ProductInfo;
import com.ecommerce.model.UserInfoModel;
import com.ecommerce.repository.UserInfoRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserInfoService {

	@Autowired
	UserInfoRepository userInfoRepository;
	
	@Autowired
	RestTemplate restTemplate;

	public UserInfo addUser(UserInfoModel userInfoModel) {
		try {
			log.info("Inside addUser Service");
			UserInfo userInfo = new UserInfo();
			BeanUtils.copyProperties(userInfoModel, userInfo);
			userInfo = userInfoRepository.save(userInfo);
			return userInfo;
		} catch (Exception e) {
			// TODO: handle exception
			log.info("In exception "+e.getMessage());
			return null;
		}

	}
	
	
	public List<UserInfo> getAllUsers(){
		return userInfoRepository.findAll();
	}

	public UserInfo getUserById(Long userId) {
		return userInfoRepository.findById(userId).orElse(null);
	}


	public List<ProductInfo> getProducts() {
		// TODO Auto-generated method stub
		HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    HttpEntity <String> entity = new HttpEntity<String>(headers);
	    
	    ResponseEntity<ProductInfo[]> responseEntity = restTemplate.exchange("http://localhost:8081/productservice/products", HttpMethod.GET, entity, ProductInfo[].class);
	    return Arrays.asList(responseEntity.getBody());
	    //  return restTemplate.exchange("http://localhost:8080/products", HttpMethod.GET, entity, ProductInfo[].class).getBody();

	}
	
	
}
